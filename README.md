# parametrized-maven-21

to use this image, run mvn goals with parameters:

```commandline
mvn -Dtoken.name=some_token_name -Dtoken.value=some_token_value  deploy
```

usefull for https://docs.gitlab.com/ee/user/packages/maven_repository/#edit-the-client-configuration