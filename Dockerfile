FROM maven:3.9.6-eclipse-temurin-21-alpine

COPY pom.xml /tmp/pom.xml
COPY /src /tmp/src
COPY settings.xml /root/.m2/
RUN mvn -B -f /tmp/pom.xml -s /root/.m2/settings.xml dependency:resolve
RUN mvn -B -f /tmp/pom.xml package
